<?php

namespace App\Repository;

use App\Entity\Motiv;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Motiv|null find($id, $lockMode = null, $lockVersion = null)
 * @method Motiv|null findOneBy(array $criteria, array $orderBy = null)
 * @method Motiv[]    findAll()
 * @method Motiv[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MotivRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Motiv::class);
    }


    // /**
    //  * @return Motiv[] Returns an array of Motiv objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Motiv
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
