<?php

namespace App\DataFixtures;

use App\Entity\Motiv;
use App\Entity\Images;
use App\Utils\MotivHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @var MotivHelper
     */
    private $motivHelper;

    /**
     * AppFixtures constructor.
     * @param MotivHelper $motivHelper
     */
    public function __construct(MotivHelper $motivHelper)
    {
        $this->motivHelper = $motivHelper;
    }


    public function load(ObjectManager $manager)
    {
        $this->loadMotiveEntrys($manager);
    }

    public function loadMotiveEntrys(ObjectManager $manager)
    {
        for($i = 0; $i < 4; $i++){
            $motiv = new Motiv();
            $subtitle = rand(1,500);
            $title = 'Random Motiv '.$subtitle;
            $publicHash = hash('ripemd160', $title);
            $url = $this->motivHelper->generate_and_check_url_if_its_unique(
                '',
                $publicHash,
                $title
            );

            $motiv->setTitle($title);
            $motiv->setSubtitle($subtitle);
            $motiv->setUserId(1);
            $motiv->setDescription('This is an random Description for random Motiv');
            $motiv->setPublicHash($publicHash);
            $motiv->setPublic(false);
            $motiv->setSeoUrl($url);
            $motiv->setUpdated(new \DateTime());


//            $image = new Images();
//            $image->setFilename('random-filename-'.rand(1,500));
//            $image->setType(1);
//            $image->setMotiv($motiv);

            $manager->persist($motiv);
//            $manager->persist($image);
        }


        $manager->flush();
    }
}
