<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Motiv;
use App\Form\MotivType;
use App\Utils\MotivHelper;

use App\Entity\Images;
use App\Form\ImageSelectionType;
use App\Form\TitleImageSelectionType;

class MotivController extends AbstractController
{
    /**
     * @var MotivHelper
     */
    private $motivHelper;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * MotivController constructor.
     * @param MotivHelper $motivHelper
     * @param FlashBagInterface $flashBag
     */
    public function __construct(MotivHelper $motivHelper, FlashBagInterface $flashBag)
    {
        $this->userId = 1;
        $this->motivHelper = $motivHelper;
        $this->flashBag = $flashBag;
    }

    /**
     * @Route("/motive", name="motive_show")
     * @return Response
     */
    public function index()
    {
        $motive = $this->getDoctrine()->getRepository(Motiv::class)->findBy(array('user_id' => $this->userId), array('id' => 'DESC'));
        foreach($motive AS $key => $motiv){
            $images = $this->getDoctrine()->getRepository(Images::class)->findBy(array('motiv' => $motiv));
            $motive[$key]->setImages($images);
        }

        return $this->render('motiv/index.html.twig',[
            'motive' => $motive
        ]);
    }

    /**
     * @Route("/motive/create", name="motiv_create")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request)
    {
        // New Motiv
        $motiv = new Motiv;
        $motiv->setImages([]);

        // Make Form for Motiv
        $form = $this->createForm(MotivType::class, $motiv);

        // Handle Request of Form Motiv
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // set UserId in Entity Motiv
            $motiv->setUserId($this->userId);

            // generate Public Hash by title and datetime
            $publicHash = hash('ripemd160', $form['title']->getData().date('YmdHis'));

            // set Public Hash in Entity Motiv
            $motiv->setPublicHash($publicHash);

            // generate and check URL and save in Entity Motiv
            $url = $this->motivHelper->generate_and_check_url_if_its_unique(
                $motiv->getSeoUrl(),
                $publicHash,
                $motiv->getTitle()
            );

            // save URL in Motiv
            $motiv->setSeoUrl($url);

            // set actually DateTime
            $motiv->setUpdated(new \DateTime());

            // Add new Entry in Database
            $em = $this->getDoctrine()->getManager();
            $em->persist($motiv);
            $em->flush();

            // Upload File and store in Database
            $this->motivHelper->uploadMotivImage(
                $form['file']->getData(),
                $motiv,
                $this->getParameter('upload_folder')
            );

            // create Flashbag Message for User
            $this->flashBag->add('notice', 'Motiv was successfull created!');

            return $this->redirectToRoute('motiv_edit', ['publicHash' => $publicHash]);
        }

        return $this->render('motiv/new.html.twig',[
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/motive/{publicHash}", name="motiv_edit")
     * @return Response
     */
    public function edit($publicHash, Request $request)
    {
        // get Motiv by UserId and MotivId
        $motiv = $this->getDoctrine()->getRepository(Motiv::class)
            ->findOneBy([
                'public_hash' => $publicHash,
                'user_id' => $this->userId
            ]);

        // If nothing find, redirect back
        if(!$motiv){ return $this->redirectToRoute('motive_show'); }

        // get the actually Url for detect changes after submit
        $oldUrl = $motiv->getSeoUrl();

        // set Public Hash in Entity Motiv
        $publicHash = $motiv->getPublicHash();

        $images = $this->getDoctrine()->getRepository(Images::class)->findBy(['motiv' => $motiv]);
        $motiv->setImages($images);
        //dump($motiv);

        // Make Form for Motiv
        $form = $this->createForm(MotivType::class, $motiv);

        // Handle Request of Form Motiv
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // generate and/or check URL
            $url = $this->motivHelper->generate_and_check_url_if_its_unique(
                $motiv->getSeoUrl(),
                $publicHash,
                $motiv->getTitle()
            );

            // save URL in Motiv
            $motiv->setSeoUrl($url);

            // set actually DateTime
            $motiv->setUpdated(new \DateTime());

            // Save Motiv in Database
            $em = $this->getDoctrine()->getManager();
            $em->persist($motiv);
            $em->flush();

            // Upload File and save in Database
            $this->motivHelper->uploadMotivImage(
                $form['file']->getData(),
                $motiv,
                $this->getParameter('upload_folder')
            );


            // create Flashbag Message for User
            $this->flashBag->add('notice', 'Motiv was successfull changed!');

            // redirect to refresh all Data and Images
            return $this->redirectToRoute('motiv_edit', ['publicHash' => $publicHash]);
        }

        // get all Images from Motiv
        $files = $this->motivHelper->getImageFiles($this->getParameter('upload_folder'), $motiv);

        return $this->render('motiv/edit.html.twig',[
            'form' => $form->createView(),
            'files' => $files,
        ]);
    }
}
