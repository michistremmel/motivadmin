<?php
namespace App\Utils;

use App\Entity\Motiv;
use App\Entity\Images;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class MotivHelper extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Here we generate an URL an check if its unique in Database,
     * if its not unique, combine this url with actually DateTime as an numeric Hash
     *
     * @param $url
     * @param $publicHash
     * @param $title
     * @return string|string[]|null
     */
    public function generate_and_check_url_if_its_unique($url,$publicHash,$title){
        // if url from form is not empty use this, else use the title and create an url
        if ($url) {
            $url = preg_replace('/[^a-zA-Z0-9\\-\\r\\n]/', '', strtolower(str_replace(" ", "-", $url)));
        } else {
            $url = preg_replace('/[^a-zA-Z0-9\\-\\r\\n]/', '', strtolower(str_replace(" ", "-", $title)));
        }

        // if find someone, check if it has the same publicHash.
        // if not the same publicHash, then generate combine the Url with an actually Timestamp to make it unique
        $findMotivByUrl = $this->em->getRepository(Motiv::class)->findOneBy(['seo_url' => $url]);
        if($findMotivByUrl && $findMotivByUrl->getPublicHash() != $publicHash) {
            $url = $url . '-' . date('YmdHis');
        }

        return $url;
    }

    /**
     * If $file exists, $motiv exists and $path exists:
     * transliterate Filename, create new Image Entity by Motiv-Object, save Image Entity in Database and Upload Image File
     *
     * @param $file
     * @param $motiv
     * @param $path
     */
    public function uploadMotivImage($file,$motiv,$path){
        if ($file && $motiv && $path) {
            // get Motiv PublicHash from Motiv-Object
            $publicHash = $motiv->getPublicHash();

            // Get original Filename from File
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

            // transliterate Filename
            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            $safeFilename = $safeFilename . '.' . $file->guessExtension();

            // Create new Image Entity
            $image = new Images();
            $image->setFilename($safeFilename);
            $image->setType(0);
            $image->setMotiv($motiv);

            // Save new Image Entity in Database
            $this->em->persist($image);
            $this->em->flush();

            // Upload Image File
            try {
                // check if an Public Hash Folder exists
                // If not, create it
                if (!is_dir($path . '/' . $publicHash)) {
                    mkdir($path . '/' . $publicHash);
                }

                $file->move(
                    $path . '/' . $publicHash,
                    $safeFilename
                );

            } catch (FileException $e) {

            }
        }
    }

    /**
     * $path = Root-Folder of Uploads
     * $motiv = Motiv-Entity-Object
     *
     * @param $path
     * @param $motiv
     * @return array
     */
    public function getImageFiles($path, $motiv) {
        $publicHash = $motiv->getPublicHash();
        $upload_dir = $path.'/'.$publicHash;

        // get all Images by Motiv
        $images = $this->em->getRepository(Images::class)->findBy(['motiv' => $motiv]);
        
        if(strpos($_SERVER['HTTP_HOST'], 'localhost') !== false){
            $http = 'http';
        }
        else {
            $http = 'https';
        }

        $i = 0;
        $files = [];
        foreach ($images as $image) {
            if($image->getFilename()){
                if (file_exists($upload_dir.'/'.$image->getFilename())) {
                    $id = $image->getId();
                    $files[$id]['id'] = $id;
                    $files[$id]['path'] = $upload_dir.'/'.$image->getFilename();
                    $files[$id]['url'] = $http.'://'.$_SERVER['HTTP_HOST'].'/uploads/'.$publicHash.'/'.$image->getFilename();
                    $files[$id]['name'] = $image->getFilename();
                    $files[$id]['type'] = $image->getType();
                    $i++;
                }
            }
        }

        return $files;
    }
}