<?php

namespace App\Form;

use App\Entity\Motiv;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;


class MotivType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titel',
                'required' => true
            ])
            ->add('subtitle', TextType::class, [
                'label' => 'Zusatzbezeichnung',
                'required' => false
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Motivbeschreibung',
                'required' => false,
                'attr' => [
                    'rows' => '6',
                ]
            ])
            ->add('seo_title', TextType::class, [
                'label' => 'SEO-Titel (wenn frei gelassen, wird der Motiv-Titel dafür verwendet)',
                'required' => false
            ])
            ->add('seo_description', TextType::class, [
                'label' => 'SEO-Beschreibungstext (optional)',
                'required' => false
            ])
            ->add('seo_url', TextType::class, [
                'label' => 'SEO-Url (wenn frei gelassen, wird der Motiv-Titel dafür verwendet)',
                'required' => false
            ])
            ->add('public', ChoiceType::class, [
                'label' => 'Motiv online?',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'choices' => [
                    'Offline' => 0,
                    'Online' => 1
                ]
            ])
            ->add('file', FileType::class, [
                'label' => 'Image-Upload (jpg, png)',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid Jpg or Png Image',
                    ])
                ]
            ])
            ->add('images', CollectionType::class, [
                'label' => false,
                'entry_type' => ImageType::class,
                'entry_options' => ['label' => false],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Speichern'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Motiv::class,
        ]);
    }
}
